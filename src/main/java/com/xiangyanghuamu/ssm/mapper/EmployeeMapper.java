package com.xiangyanghuamu.ssm.mapper;

import com.xiangyanghuamu.ssm.pojo.Employee;

import java.util.List;

public interface EmployeeMapper {

    /**
     * 查询所有员工信息
     * @return
     */
    List<Employee> getALLEmployee();

    /**
     * 根据id查询员工信息
     * @param empId
     * @return
     */
    Employee getEmployeeById(Integer empId);


    /**
     * 添加员工信息
     * @return
     */
    int addEmployee(Employee emoloyee);

    /**
     * 修改员工信息
     * @param emoloyee
     * @return
     */
    int updateEmployee(Employee emoloyee);

    /**
     * 删除员工信息
     * @param empId
     * @return
     */
    int deleteEmployee(Integer empId);
}
