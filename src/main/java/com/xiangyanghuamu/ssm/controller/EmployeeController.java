package com.xiangyanghuamu.ssm.controller;

import com.github.pagehelper.PageInfo;
import com.xiangyanghuamu.ssm.pojo.Employee;
import com.xiangyanghuamu.ssm.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/*
查询所有的员工信息-->/employee-->get
查询所有的员工的分页信息-->/employee/page/1-->get
根据id查询员工信息-->/employee/1-->get
跳转到添加页面-->/to/add-->get
添加员工信息-->/employee-->post
修改员工信息-->/employee-->put
删除员工信息-->/employee/1-->delete
* */
@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value="/employee/page/{pageNum}",method = RequestMethod.GET)
    public String getEmployeePage(@PathVariable("pageNum") Integer pageNum,Model model){
        //获取员工的分页信息
        PageInfo<Employee> page = employeeService.getEmployeePage(pageNum);
        //将员工的分页信息在请求域中共享
        model.addAttribute("page",page);
        //跳转到employee_list.html
        return "employee_list";
    }
    @RequestMapping(value = "/employee",method = RequestMethod.GET)
    public String getAllEmployee(Model model){
        //查询所有的员工信息
        List<Employee> list = employeeService.getAllEmployee();
       //将员工信息在请求域中共享
        model.addAttribute("list",list);
        //跳转到employee_list.html
        return "employee_list";
    }

    @RequestMapping(value = "/employeeId",method = RequestMethod.GET)
    public String getEmployeeById(Integer empId,Model model){
        //根据id查询员工信息
        Employee employee = employeeService.getEmployeeById(empId);
        //将员工信息在请求域中共享
        model.addAttribute(employee);
        //跳转到employee_info页面
        return "employee_info";
    }

    @RequestMapping(value="/to/add",method=RequestMethod.GET)
    public String toEmployee(){
        return "employee_add";
    }

    @RequestMapping(value="/success",method=RequestMethod.POST)
    public String addEmployee(Employee emoloyee){
        //添加员工信息
        int status = employeeService.addEmployee(emoloyee);
        if(status == 1){
            return "success";
        }
        return "failure";
    }

    @RequestMapping(value="/employee/{empId}",method=RequestMethod.GET)
    public String toUpdate(@PathVariable("empId") Integer empId,Model model){
        Employee employee = employeeService.getEmployeeById(empId);
        model.addAttribute("employee",employee);
        return "employee_update";
    }
    @RequestMapping(value="/employee",method = RequestMethod.PUT)
    public String updateEmployee(Employee employee){
        //修改员工信息
        int status = employeeService.updateEmployee(employee);
        if(status == 1){
            return "redirect:/employee/page/1";
        }
        return "failure";
    }

    @RequestMapping(value="/employee/{empId}",method = RequestMethod.DELETE)
    public String deleteEmployee(@PathVariable("empId") Integer empId){
        //删除员工信息
        int status = employeeService.deleteEmployee(empId);
        if(status == 1){
            return "redirect:/employee/page/1";
        }
        return "failure";
    }
}
