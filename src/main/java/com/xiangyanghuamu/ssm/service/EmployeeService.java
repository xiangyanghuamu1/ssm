package com.xiangyanghuamu.ssm.service;

import com.github.pagehelper.PageInfo;
import com.xiangyanghuamu.ssm.pojo.Employee;

import java.util.List;

public interface EmployeeService {

    /**
     * 查询所有员工信息
     * @return
     */
    List<Employee> getAllEmployee();

    /**
     * 获取员工的分页信息
     * @param pageNum
     * @return
     */
    PageInfo<Employee> getEmployeePage(Integer pageNum);

    /**
     * 根据id查询员工信息
     * @return
     */
    Employee getEmployeeById(Integer empId);

    /**
     * 添加员工信息
     * @return
     */
    int addEmployee(Employee employee);

    /**
     * 修改员工信息
     * @param employee
     * @return
     */
    int updateEmployee(Employee employee);

    /**
     * 删除员工信息
     * @param empId
     * @return
     */
    int deleteEmployee(Integer empId);
}
