package com.xiangyanghuamu.ssm.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xiangyanghuamu.ssm.mapper.EmployeeMapper;
import com.xiangyanghuamu.ssm.pojo.Employee;
import com.xiangyanghuamu.ssm.service.EmployeeService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public List<Employee> getAllEmployee() {
        return employeeMapper.getALLEmployee();
    }

    public PageInfo<Employee> getEmployeePage(Integer pageNum) {
        //开启分页功能
        PageHelper.startPage(pageNum,4);
        //查询所有的员工信息
        List<Employee> list = employeeMapper.getALLEmployee();
        //获取分页相关数据
        PageInfo<Employee> page = new PageInfo<>(list, 5);
        return page;
    }

    @Override
    public Employee getEmployeeById(Integer empId) {
        Employee employee = employeeMapper.getEmployeeById(empId);
        return employee;
    }

    @Override
    public int addEmployee(Employee employee) {
        int status = employeeMapper.addEmployee(employee);
        return status;
    }

    @Override
    public int updateEmployee(Employee employee) {
        int status = employeeMapper.updateEmployee(employee);
        return status;
    }

    @Override
    public int deleteEmployee(Integer empId) {
        int status = employeeMapper.deleteEmployee(empId);
        return status;
    }


}
